<div class="container-fluid p-0">
    <div class="animation-element"></div>
    <div class="container my-md-2 my-0">
        <div class="row">
            <div class="col-md-4">
                <div id="logo" class="d-none d-md-block">
                    <a href="/"><img class="img-fluid" src="{{ asset('img/logo.png') }}" alt="{{ $host_name }}" /></a>
                </div>
            </div>
            <div class="col-md-8">
                <div id="ads-header">
                    <a href="#"><img class="img-fluid" src="{{ asset('img/ads.jpg') }}" alt="" /></a>
                    <button type="button" class="close d-md-none" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<nav id="navbar" class="navbar navbar-expand-lg navbar-dark bg-dark sidebarNavigation" data-sidebarClass="navbar-dark bg-dark">
    <div class="container-fluid">
        <a href="/" class="second-brand d-flex justify-content-center d-md-none mb-0">
            <img src="{{ asset('/img/admin/logo-nav.png') }}" alt="{{ $host_name }} Logo" class="brand-image rounded-circle elevation-3">
            <span class="brand-text font-weight-light text-uppercase">Poraman</span>
        </a>
        <button class="navbar-toggler leftNavbarToggler" type="button" data-toggle="" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <button type="button" class="close d-lg-none" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <a href="/" class="second-brand d-flex justify-content-center d-lg-none mb-5">
                <img src="{{ asset('/img/admin/logo-nav.png') }}" alt="{{ $host_name }} Logo" class="brand-image rounded-circle elevation-3">
                <span class="brand-text font-weight-light text-uppercase">Poraman</span>
            </a>
            <ul class="nav navbar-nav nav-flex-icons mr-auto">
                <li class="nav-item pl-0">
                    <a class="nav-link pl-0" href="/">
                        <div>
                            <i class="{{ config('global.icon_home') }}"></i>
                            <span data-hover="ទំព័រដើម">ទំព័រដើម</span>
                        </div>
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <div>
                            <i class="{{ config('global.icon_entertainment') }} d-lg-none d-xl-inline-block"></i>
                            <span data-hover="កម្សាន្ត">កម្សាន្ត</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <div>
                            <i class="{{ config('global.icon_chakdoat') }} d-lg-none d-xl-inline-block"></i>
                            <span data-hover="ចាក់ដោត">ចាក់ដោត</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <div>
                            <i class="{{ config('global.icon_social') }} d-lg-none d-xl-inline-block"></i>
                            <span data-hover="របត់សង្គម">របត់សង្គម</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <div>
                            <i class="{{ config('global.icon_beauty') }} d-lg-none d-xl-inline-block"></i>
                            <span data-hover="សោភ័ណ">សោភ័ណ</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <div>
                            <i class="{{ config('global.icon_abstract') }} d-lg-none d-xl-inline-block"></i>
                            <span data-hover="អរូបី">អរូបី</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <div>
                            <i class="{{ config('global.icon_pr') }} d-lg-none d-xl-inline-block"></i>
                            <span data-hover="PR">PR</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <div>
                            <i class="{{ config('global.icon_radio') }} d-lg-none d-xl-inline-block"></i>
                            <span data-hover="ល្ខោនវិទ្យុ">ល្ខោនវិទ្យុ</span>
                        </div>
                    </a>
                </li>
            </ul><!-- .navbar-nav -->
            <!-- social mobile -->
            <ul class="social d-lg-none">
                <li class="nav-item mx-2">
                    <a href="https://www.facebook.com/PoramanCambodia" target="_blank" class="icon-button facebook"><i class="fab fa-facebook-f"></i><span></span></a>
                </li>
                <li class="nav-item mx-2">
                    <a href="https://www.youtube.com/c/PoramanCambodiaOfficial" target="_blank" class="icon-button bg-youtube youtube"><i class="fab fa-youtube"></i><span></span></a>
                </li>
                <li class="nav-item mx-2">
                    <a href="https://www.instagram.com/PoramanCambodia" target="_blank" class="icon-button instagram"><i class="fab fa-instagram"></i><span></span></a>
                </li>
            </ul>
        </div><!-- #navbarsExampleDefault -->

        <div class="nav-right">
            <ul class="social d-inline-block m-0 p-0">
                <li class="nav-item">
                    <a href="https://www.facebook.com/PoramanCambodia" target="_blank" class="icon-button facebook"><i class="fab fa-facebook-f"></i><span></span></a>
                </li>
                <li class="nav-item">
                    <a href="https://www.youtube.com/c/PoramanCambodiaOfficial" target="_blank" class="icon-button bg-youtube youtube"><i class="fab fa-youtube"></i><span></span></a>
                </li>
            </ul>
            <ul class="d-inline-block m-0 p-0">
                <li class="nav-item pr-0">
                    <button id="search-btn"><i class="fas fa-search"></i></button>
                </li>
            </ul>
        </div>

    </div>
</nav>

<!-- search overlay -->
<div class="search-overlay">
    <button id="close-btn" type="button" class="close" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <form action="">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 col-sm-12 offset-sm-0">
                    <div class="input-group form-btn">
                        <input type="text" name="txt_search" class="form-control border-bottom" placeholder="Search here..." aria-describedby="button-submit">
                        <div class="input-group-append">
                            <button class="btn btn-outline-light brd-left-none border-bottom" type="button" id="button-submit"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

