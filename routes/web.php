<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// define global variable
view()->share('version', time());
view()->share('host_name', '855Lottery');

// Auth
Auth::routes();

// Redirect to Dashboard
Route::get('/', function() {
    return redirect('/dashboard');
});
// Permission Access
Route::group(['middleware' => 'auth'], function()
{
    // Admin
    Route::get('/dashboard', 'admin\DashboardController@index')->name('dashboard');

    // User
    Route::get('/user', 'admin\UserController@index')->name('user');
});



